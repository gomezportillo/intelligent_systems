# DOCUMENTATION #

Python program to generate a graph into a hash table from a .osm file and work within it.

## AUTHORS ##

Juan Garrido Arcos 
     -  *juan.garrido3@alu.uclm.es*
    
Pedro-Manuel Gómez-Portillo López 
     -  *pedromanuel.gomezportillo@alu.uclm.es*

## MANUAL ##

Should you want to execute any milestone, a Makefile has been writen for this purpose.


```
#!sh
make
```
      build: to download the .osm file of the map of Ciudad Real
      test<n>: for executing different test the program with a right node
      gui: for executing the grafical user interface (only on milestone4_gui)
      test_error: for executing the program with an invalid node
      clean: for deleting the binaries and temporaly files





![GUI](https://bytebucket.org/pedroma-gomezp/intelligent_systems/raw/1d61672757438a4165de2047c69e7210d2a6cb5b/milestone4_gui/doc/GUI.png?token=56b64b01eede4e4c65e400345ba178bda2fe1383)